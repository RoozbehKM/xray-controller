FROM golang:1.20-bullseye

COPY . /app
RUN go mod tidy && go build -o /usr/local/bin/xray-controller ./cmd/xray-controller.go
