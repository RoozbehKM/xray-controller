package helper

import (
	"os"
	"regexp"
	"strconv"
	"strings"
)

func RemoveIndex(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}

func ListenCommand(name string) bool {
	if len(os.Args) < 2 {
		return false
	}
	if os.Args[1] == name {
		return true
	}
	return false
}

func GetInput(name string, defaultValue interface{}) interface{} {
	if len(os.Args) < 2 {
		return nil
	}
	args := os.Args
	for _, arg := range args {
		match, _ := regexp.MatchString("^--"+name, arg)
		if match {
			return strings.Split(arg, "=")[1]
		}
	}
	return defaultValue
}

func StringToUint16(data string) uint16 {
	intData, err := strconv.Atoi(data)
	if err != nil {
		return 0
	}

	return uint16(intData)
}

func GetHostAndPortFromString(url string) (string, uint16) {
	server := strings.Split(url, ":")
	host := server[0]
	port := StringToUint16(server[1])

	return host, port
}
