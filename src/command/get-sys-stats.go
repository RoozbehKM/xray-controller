package command

import (
	"encoding/json"
	"fmt"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src/domain/stats"
)

type sysDetails struct {
	NumGoroutine uint32 `json:"numGoroutine,omitempty"`
	NumGC        uint32 `json:"numGC,omitempty"`
	Alloc        uint64 `json:"alloc,omitempty"`
	TotalAlloc   uint64 `json:"totalAlloc,omitempty"`
	Sys          uint64 `json:"sys,omitempty"`
	Mallocs      uint64 `json:"mallocs,omitempty"`
	Frees        uint64 `json:"frees,omitempty"`
	LiveObjects  uint64 `json:"liveObjects,omitempty"`
	PauseTotalNs uint64 `json:"pauseTotalNs,omitempty"`
	Uptime       uint32 `json:"uptime,omitempty"`
}

func GetSysStats(server string) {
	if helper.ListenCommand("get-sys-stats") {
		host, port := helper.GetHostAndPortFromString(server)
		data, err := stats.GetSysStats(host, port)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}

		outPut := &sysDetails{
			NumGoroutine: data.NumGoroutine,
			NumGC:        data.NumGC,
			Alloc:        data.Alloc,
			TotalAlloc:   data.TotalAlloc,
			Sys:          data.Sys,
			Mallocs:      data.Mallocs,
			Frees:        data.Frees,
			LiveObjects:  data.LiveObjects,
			PauseTotalNs: data.PauseTotalNs,
			Uptime:       data.Uptime,
		}
		jsonData, err := json.Marshal(outPut)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}
		fmt.Println(string(jsonData))
	}
}
