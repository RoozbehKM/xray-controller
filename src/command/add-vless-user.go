package command

import (
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src"
	"gitlab.com/RoozbehKM/xray-controller/src/domain/proxyman"
)

func AddVlessUser(servers []string) {
	if helper.ListenCommand("add-vless-user") {
		tag := helper.GetInput("tag", "").(string)
		id := helper.GetInput("id", "").(string)
		levelStringToInt, _ := strconv.Atoi(helper.GetInput("level", "0").(string))
		level := uint32(levelStringToInt)
		email := helper.GetInput("email", "").(string)
		flow := helper.GetInput("flow", "").(string)
		alterIdStringToInt, _ := strconv.Atoi(helper.GetInput("alter-id", "0").(string))
		alterID := uint32(alterIdStringToInt)

		status := make(map[string]src.ExecuteStatus)
		for _, server := range servers {
			host, port := helper.GetHostAndPortFromString(server)
			err := proxyman.AddUser(host, port, tag, level, id, email, alterID, "vless", &flow)
			if err != nil {
				status[server] = src.ExecuteStatus{
					Error:  fmt.Sprintf("Failed %s", err),
					Status: false,
				}
				// fmt.Printf("Failed %s", err)
			} else {
				status[server] = src.ExecuteStatus{
					Error:  "",
					Status: true,
				}
			}
		}

		jsonData, err := json.Marshal(status)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}
		fmt.Println(string(jsonData))
	}
}
