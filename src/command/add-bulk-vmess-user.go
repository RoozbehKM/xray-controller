package command

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src"
	"gitlab.com/RoozbehKM/xray-controller/src/domain/proxyman"
)

func AddBulkVmessUser(servers []string) {
	if helper.ListenCommand("add-bulk-vmess-user") {
		intro := ""
		intro = intro + "You can enter json of user data, e.g.\n"
		intro = intro + "[{\"id\": \"User's UUID\", \"email\": \"User's Email\", \"level\": 1, \"alterId\": 0}]\n\n"
		intro = intro + "wait for STDIN ...\n"
		os.Stdout.WriteString(intro)

		tag := helper.GetInput("tag", "").(string)

		var users []src.UserInfo
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			// fmt.Println(scanner.Text())
			data := scanner.Bytes()
			err := json.Unmarshal(data, &users)
			if err != nil {
				fmt.Printf("error %v\n", err)
				continue
			}
			break
		}

		status := make(map[string]src.ExecuteStatus)
		for _, server := range servers {
			host, port := helper.GetHostAndPortFromString(server)
			err := proxyman.AddBulkUser(host, port, tag, "vmess", users, nil)
			if err != nil {
				status[server] = src.ExecuteStatus{
					Error:  fmt.Sprintf("Failed %s", err),
					Status: false,
				}
			} else {
				status[server] = src.ExecuteStatus{
					Error:  "",
					Status: true,
				}
			}
		}

		jsonData, err := json.Marshal(status)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}
		os.Stdout.WriteString(string(jsonData))
	}
}
