package command

import (
	"encoding/json"
	"fmt"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src/domain/stats"
)

func GetAllQueryStats(servers []string) {
	if helper.ListenCommand("get-all-query-stats") {
		resetInput := helper.GetInput("reset", "").(string)
		reset := false
		if resetInput == "true" || resetInput == "1" {
			reset = true
		}

		outPut := make(map[string]stats.Stats)
		for _, server := range servers {
			host, port := helper.GetHostAndPortFromString(server)

			data, err := stats.GetQueryStats(host, port, reset)
			if err != nil {
				fmt.Printf("Failed %s", err)
				return
			}
			outPut[server] = *data
		}

		jsonData, err := json.Marshal(outPut)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}
		fmt.Println(string(jsonData))
	}
}
