package command

import (
	"encoding/json"
	"fmt"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src"
	"gitlab.com/RoozbehKM/xray-controller/src/domain/proxyman"
)

func RemoveUser(servers []string) {
	if helper.ListenCommand("remove-user") {
		tag := helper.GetInput("tag", "").(string)
		email := helper.GetInput("email", "").(string)

		status := make(map[string]src.ExecuteStatus)
		for _, server := range servers {
			host, port := helper.GetHostAndPortFromString(server)
			err := proxyman.RemoveUser(host, port, tag, email)
			if err != nil {
				status[server] = src.ExecuteStatus{
					Error:  fmt.Sprintf("Failed %s", err),
					Status: false,
				}
				// fmt.Printf("Failed %s", err)
			} else {
				status[server] = src.ExecuteStatus{
					Error:  "",
					Status: true,
				}
			}
		}

		jsonData, err := json.Marshal(status)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}
		fmt.Println(string(jsonData))
	}
}
