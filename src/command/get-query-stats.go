package command

import (
	"encoding/json"
	"fmt"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src/domain/stats"
)

func GetQueryStats(host string, port uint16) {
	if helper.ListenCommand("get-query-stats") {
		resetInput := helper.GetInput("reset", "").(string)
		reset := false
		if resetInput == "true" || resetInput == "1" {
			reset = true
		}
		data, err := stats.GetQueryStats(host, port, reset)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}

		jsonData, err := json.Marshal(data)
		if err != nil {
			fmt.Printf("Failed %s", err)
			return
		}
		fmt.Println(string(jsonData))
	}
}
