package src

import (
	loggerService "github.com/xtls/xray-core/app/log/command"
	"github.com/xtls/xray-core/app/proxyman/command"
	routingService "github.com/xtls/xray-core/app/router/command"
	statsService "github.com/xtls/xray-core/app/stats/command"
	"google.golang.org/grpc"
)

// Xray API
type BaseConfig struct {
	APIAddress string
	APIPort    uint16
}

type UserInfo struct {
	// For VMess & Trojan
	Uuid string `json:"id"`
	// For VMess
	AlertId uint32 `json:"alterId"`
	Level   uint32 `json:"level"`
	// Which Inbound will add this user
	InTag string `json:"tag"`
	// User's Email, it's a unique identifier for users
	Email string `json:"email"`
	// For ShadowSocks
	CipherType string
	// For ShadowSocks
	Password string
}

// Xray API
type XrayController struct {
	HsClient command.HandlerServiceClient
	SsClient statsService.StatsServiceClient
	LsClient loggerService.LoggerServiceClient
	RsClient routingService.RoutingServiceClient
	CmdConn  *grpc.ClientConn
}

type ExecuteStatus struct {
	Status bool   `json:"status"`
	Error  string `json:"error"`
}
