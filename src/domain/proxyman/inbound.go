package proxyman

import (
	"context"

	"github.com/xtls/xray-core/app/proxyman"
	"github.com/xtls/xray-core/app/proxyman/command"
	"github.com/xtls/xray-core/common/net"
	"github.com/xtls/xray-core/common/protocol"
	"github.com/xtls/xray-core/common/protocol/tls/cert"
	"github.com/xtls/xray-core/common/serial"
	"github.com/xtls/xray-core/core"
	"github.com/xtls/xray-core/proxy/vmess"
	vmessInbound "github.com/xtls/xray-core/proxy/vmess/inbound"
	"github.com/xtls/xray-core/transport/internet"
	"github.com/xtls/xray-core/transport/internet/tls"
	"github.com/xtls/xray-core/transport/internet/websocket"
)

func addInbound(client command.HandlerServiceClient, tag, protocolName, path string) error {
	_, err := client.AddInbound(context.Background(), &command.AddInboundRequest{
		Inbound: &core.InboundHandlerConfig{
			Tag: tag,
			ReceiverSettings: serial.ToTypedMessage(&proxyman.ReceiverConfig{
				PortRange: net.SinglePortRange(net.Port(12360)),
				Listen:    net.NewIPOrDomain(net.AnyIP),
				SniffingSettings: &proxyman.SniffingConfig{
					Enabled:             true,
					DestinationOverride: []string{"http", "tls"},
				},
				StreamSettings: &internet.StreamConfig{
					ProtocolName: protocolName,
					TransportSettings: []*internet.TransportConfig{
						{
							ProtocolName: protocolName,
							Settings: serial.ToTypedMessage(&websocket.Config{
								Path: path,
								Header: []*websocket.Header{
									{
										Key:   "Host",
										Value: "www.xray.best",
									},
								},
								AcceptProxyProtocol: false,
							},
							),
						},
					},
					SecurityType: serial.GetMessageType(&tls.Config{}),
					SecuritySettings: []*serial.TypedMessage{
						serial.ToTypedMessage(&tls.Config{
							Certificate: []*tls.Certificate{tls.ParseCertificate(cert.MustGenerate(nil))},
						}),
					},
				},
			}),
			ProxySettings: serial.ToTypedMessage(&vmessInbound.Config{
				User: []*protocol.User{
					{
						Level: 0,
						Email: "love@xray.com",
						Account: serial.ToTypedMessage(&vmess.Account{
							Id:      "10354ac4-9ec1-4864-ba3e-f5fd35869ef8",
							AlterId: 1,
						}),
					},
				},
			}),
		},
	})

	return err
}
