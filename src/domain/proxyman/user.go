package proxyman

import (
	"context"

	"github.com/xtls/xray-core/app/proxyman/command"
	"github.com/xtls/xray-core/common/protocol"
	"github.com/xtls/xray-core/common/serial"
	"github.com/xtls/xray-core/proxy/vless"
	"github.com/xtls/xray-core/proxy/vmess"
	"gitlab.com/RoozbehKM/xray-controller/src"
)

func addVmessUser(client command.HandlerServiceClient, user *src.UserInfo) error {
	request := &command.AlterInboundRequest{
		Tag: user.InTag,
		Operation: serial.ToTypedMessage(&command.AddUserOperation{
			User: &protocol.User{
				Level: user.Level,
				Email: user.Email,
				Account: serial.ToTypedMessage(&vmess.Account{
					Id:      user.Uuid,
					AlterId: user.AlertId,
				}),
			},
		}),
	}
	_, err := client.AlterInbound(context.Background(), request)
	return err
}

func addVlessUser(client command.HandlerServiceClient, user *src.UserInfo, flow *string) error {
	var vlessAccount vless.Account
	vlessAccount.Id = user.Uuid
	if flow != nil {
		vlessAccount.Flow = *flow
	}
	request := &command.AlterInboundRequest{
		Tag: user.InTag,
		Operation: serial.ToTypedMessage(&command.AddUserOperation{
			User: &protocol.User{
				Level:   user.Level,
				Email:   user.Email,
				Account: serial.ToTypedMessage(&vlessAccount),
			},
		}),
	}
	_, err := client.AlterInbound(context.Background(), request)
	return err
}

func removeUser(client command.HandlerServiceClient, user *src.UserInfo) error {
	request := &command.AlterInboundRequest{
		Tag: user.InTag,
		Operation: serial.ToTypedMessage(&command.RemoveUserOperation{
			Email: user.Email,
		}),
	}
	_, err := client.AlterInbound(context.Background(), request)
	return err
}

func AddBulkUser(host string, port uint16, tag, proxyType string, users []src.UserInfo, flow *string) error {
	var (
		xrayCtl *src.XrayController
		cfg     = &src.BaseConfig{
			APIAddress: host,
			APIPort:    port,
		}
	)
	xrayCtl = new(src.XrayController)
	err := xrayCtl.Init(cfg)
	defer xrayCtl.CmdConn.Close()
	if err != nil {
		return err
	}
	for _, user := range users {
		if user.InTag == "" {
			user.InTag = tag
		}

		if proxyType == "vmess" {
			addVmessUser(xrayCtl.HsClient, &user)
		} else if proxyType == "vless" {
			addVlessUser(xrayCtl.HsClient, &user, flow)
		}
	}
	return nil
}

func AddUser(host string, port uint16, tag string, level uint32, id, email string, alterID uint32, proxyType string, flow *string) error {
	var users []src.UserInfo
	users = append(users, src.UserInfo{
		Uuid:    id,
		AlertId: alterID,
		Level:   level,
		InTag:   tag,
		Email:   email,
	})
	err := AddBulkUser(host, port, tag, proxyType, users, flow)
	if err != nil {
		return err
	}
	return nil
}

func RemoveUser(host string, port uint16, tag, email string) error {
	var (
		xrayCtl *src.XrayController
		cfg     = &src.BaseConfig{
			APIAddress: host,
			APIPort:    port,
		}
		user = src.UserInfo{
			InTag: tag,
			Email: email,
		}
	)
	xrayCtl = new(src.XrayController)
	err := xrayCtl.Init(cfg)
	defer xrayCtl.CmdConn.Close()
	if err != nil {
		return err
	}

	err = removeUser(xrayCtl.HsClient, &user)
	if err != nil {
		return err
	}
	return nil
}
