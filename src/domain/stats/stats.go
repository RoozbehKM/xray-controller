package stats

import (
	"context"
	"strings"

	statsService "github.com/xtls/xray-core/app/stats/command"
	"gitlab.com/RoozbehKM/xray-controller/src"
)

func getSysStats(c statsService.StatsServiceClient) (stats *statsService.SysStatsResponse, err error) {
	stats, err = c.GetSysStats(context.Background(), &statsService.SysStatsRequest{})

	return
}

func getQueryStats(c statsService.StatsServiceClient, reset bool) (stats *statsService.QueryStatsResponse, err error) {
	stats, err = c.QueryStats(context.Background(), &statsService.QueryStatsRequest{
		Reset_: reset,
	})

	return
}

func GetSysStats(host string, port uint16) (*statsService.SysStatsResponse, error) {
	var (
		xrayCtl *src.XrayController
		cfg     = &src.BaseConfig{
			APIAddress: host,
			APIPort:    port,
		}
	)
	xrayCtl = new(src.XrayController)
	err := xrayCtl.Init(cfg)
	defer xrayCtl.CmdConn.Close()
	if err != nil {
		return nil, err
	}
	SysStats, err := getSysStats(xrayCtl.SsClient)
	if err != nil {
		return nil, err
	}
	return SysStats, nil
}

type UserStat struct {
	Uplink   int64 `json:"uplink"`
	Downlink int64 `json:"downlink"`
	Total    int64 `json:"total"`
}

type Stats map[string]UserStat

func GetQueryStats(host string, port uint16, reset bool) (*Stats, error) {
	var (
		xrayCtl *src.XrayController
		cfg     = &src.BaseConfig{
			APIAddress: host,
			APIPort:    port,
		}
	)
	xrayCtl = new(src.XrayController)
	err := xrayCtl.Init(cfg)
	defer xrayCtl.CmdConn.Close()
	if err != nil {
		return nil, err
	}

	QueryStats, err := getQueryStats(xrayCtl.SsClient, reset)
	if err != nil {
		return nil, err
	}

	doc := make(Stats)
	oldUserStruct := make(map[string]UserStat)
	for _, stat := range QueryStats.GetStat() {
		if strings.Contains(stat.Name, "user>>>") {
			user := strings.Split(stat.Name, ">>>")
			_, ok := oldUserStruct[user[1]]
			if !ok {
				oldUserStruct[user[1]] = UserStat{
					Uplink:   0,
					Downlink: 0,
					Total:    0,
				}
			}

			tmpData := oldUserStruct[user[1]]
			if user[3] == "uplink" {
				tmpData.Uplink = stat.Value
			}
			if user[3] == "downlink" {
				tmpData.Downlink = stat.Value
			}
			oldUserStruct[user[1]] = tmpData
			if ok {
				doc[user[1]] = UserStat{
					Uplink:   oldUserStruct[user[1]].Uplink,
					Downlink: oldUserStruct[user[1]].Downlink,
					Total:    oldUserStruct[user[1]].Uplink + oldUserStruct[user[1]].Downlink,
				}
			}
		}
	}
	return &doc, nil
}
