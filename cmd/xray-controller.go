package main

import (
	"strings"

	"gitlab.com/RoozbehKM/xray-controller/helper"
	"gitlab.com/RoozbehKM/xray-controller/src/command"
)

func main() {
	servers := strings.Split(helper.GetInput("servers", "127.0.0.1:8080, 127.0.0.1:8081").(string), ",")

	registerUserCommands(servers)
	registerStatsCommands(servers)
}

func registerUserCommands(servers []string) {
	command.AddVmessUser(servers)
	command.AddVlessUser(servers)
	command.AddBulkVmessUser(servers)
	command.AddBulkVlessUser(servers)
	command.RemoveUser(servers)
}

func registerStatsCommands(servers []string) {
	command.GetSysStats(servers[0])
	// command.GetQueryStats(servers)
	command.GetAllQueryStats(servers)
}
